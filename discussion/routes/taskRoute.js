const express = require('express')
const { route } = require('express/lib/application')
const router = express.Router()
const taskController = require('../controllers/taskController')

router.get('/', (request, response) => {
    taskController.getAll().then(result => {
        response.send(result)
    })
})

router.post('/create', (request, response) => {
    taskController.createOne(request.body).then(result => {
        response.send(result)
    })
})

router.delete('/:id/delete', (request, response) => {
    taskController.deleteOne(request.params.id).then(result => {
        response.send(result)
    })
})

router.patch('/:id/update-status-complete', (request, response) => {
    taskController.updateStatusComplete(request.params.id).then(result => {
        response.send(result)
    })
})

router.patch('/:id/update-status-pending', (request, response) => {
    taskController.updateStatusPending(request.params.id).then(result => {
        response.send(result)
    })
})

module.exports = router