const express = require("express")
const bodyParser = require("body-parser")
const mongoose = require("mongoose")
const taskRoute = require('./routes/taskRoute')

const port = 4000

const application = express()

application.use(bodyParser.urlencoded({ extended: false }));
application.use(bodyParser.json());


// mongoose connection
mongoose.connect('mongodb+srv://earlrafael:2424Diaz!@cluster0.choag.mongodb.net/myFirstDatabase?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true 
})

let database = mongoose.connection
database.on('error', console.error.bind(console, 'Connection Error'))
database.once('open', () => console.log('Connected to MongoDB'))

// Use taskRoute
application.use('/tasks', taskRoute)

application.listen(port, () => console.log(`Express server is running on port ${port}`))