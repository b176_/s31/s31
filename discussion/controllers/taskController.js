const Task = require('../models/Task')

module.exports.getAll = () => {
    return Task.find({}).then(result => {
        return result
    })
}

module.exports.createOne = (task) => {
    let new_task = new Task({
        name: task.name
    })

    return new_task.save().then((saved_task, error) => {
        if(error) {
            return false
        }else{
            return saved_task
        }        
    })
}

module.exports.deleteOne = (task_id) => {
    return Task.findByIdAndDelete(task_id).then((fulfilled, rejected) => {
        if(fulfilled) {
            return 'The task has been removed successfully!'
        } else {
            return 'Failed to remove task.'
        }
    })
}

module.exports.updateStatusComplete = (task_id) => {
    return Task.findById(task_id).then((task, error) => {
        if(task)
        {
            task.status = 'Completed'

            return task.save().then((updated_task, error) => {
                if (updated_task) {
                    return 'Task status has been updated!'
                } else {
                    return 'Task failed to update.'
                }
            })
        } else {
            return 'Error! No match found.'
        }
    })
}

module.exports.updateStatusPending = (task_id) => {
    return Task.findById(task_id).then((task, error) => {
        if(task)
        {
            task.status = 'Pending'

            return task.save().then((updated_task, error) => {
                if (updated_task) {
                    return 'Task status has been updated!'
                } else {
                    return 'Task failed to update.'
                }
            })
        } else {
            return 'Error! No match found.'
        }
    })
}